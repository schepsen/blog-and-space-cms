## PROJECT ##

* ID: **B**log & **S**pace CMS
* Contact: development@schepsen.eu

## USAGE ##

To use this project configure it at first

## REQUIREMENTS ##

* PHP 5.6.18 http://php.net/downloads.php

## CHANGELOG ##

### Blog & Space CMS 0.4.0, updated @ 2016-02-10 ###

* Switched to the Eloquent ORM

### Blog & Space CMS 0.3.1, updated @ 2014-09-01 ###

* Added a simple issue tracker
* Created a branch for small fixes only

### Blog & Space CMS 0.3.0, updated @ 2014-08-28 ###

* Switched to the TWIG as the main Template Engine

### Blog & Space CMS 0.2.1, updated @ 2014-08-20 ###

* Added Core & ACP (Admin Control Panel)

### Blog & Space CMS 0.2.0, updated @ 2014-06-04 ###

* Reopened

### Blog & Space CMS 0.1.0, Initial Release ###

* Implemented primarily for the MMMOCORE.eu TEAM (Lineage 2)