DROP TABLE IF EXISTS `usergroup`;

CREATE TABLE IF NOT EXISTS `usergroup` (

  `id` TINYINT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) NOT NULL,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `usergroup`(`name`) VALUES ('Banned');
INSERT INTO `usergroup`(`name`) VALUES ('Guests');
INSERT INTO `usergroup`(`name`) VALUES ('Members');
INSERT INTO `usergroup`(`name`) VALUES ('Moderators');
INSERT INTO `usergroup`(`name`) VALUES ('Administrators');