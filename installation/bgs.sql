-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 10. Feb 2016 um 20:58
-- Server Version: 5.5.47-0ubuntu0.14.04.1
-- PHP-Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `bgs`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `access`
--

CREATE TABLE IF NOT EXISTS `access` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` tinyint(3) NOT NULL,
  `group_id` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Daten für Tabelle `access`
--

INSERT INTO `access` (`id`, `menu_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 2),
(7, 3, 2),
(8, 4, 1),
(9, 4, 3),
(10, 4, 4),
(11, 4, 5),
(12, 5, 3),
(13, 5, 4),
(14, 5, 5),
(15, 6, 5);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` tinyint(3) unsigned NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` smallint(5) unsigned NOT NULL,
  `views` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` enum('N','Y') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `article`
--

INSERT INTO `article` (`id`, `title`, `category_id`, `text`, `user_id`, `views`, `hidden`, `created_at`, `updated_at`) VALUES
(1, 'Sortieren #0: Einleitung', 2, 'Im Rahmen der Wiederholung des Vorlesungsstoffes werde ich mir nochmal die wichtigsten Sortierverfahren anschauen. Zu Beginn nehme ich elementare Sortierverfahren mit quadratischer Laufzeit, wie z.B.\r\n<ul class="notice"><li><a href="">Bubble Sort</a> (1), Insertion Sort (2) und Selection Sort (3)</li></ul>auseinander. Dabei werde ich nicht nur auf die Laufzeit eingehen und die Anzahl der Vergleich-und Vertausch Operationen berechnen, sondern wir gucken gemeinsam Implementierungen in C an, um deren Funktionsweise besser nachzuvollziehen. Um die Theorie etwas lebendiger zu gestalten, lasse ich verschiedene Implementierungen und Verfahren gegeneinander antreten, um deren Stärken live zu sehen und das mögliche Einsatzgebiet festzulegen.\r\n\r\nIn weiteren Reviews dieser Folge werde ich weitere vergleichsbasieren Sortierverfahren wie Quicksort (4) und Mergesort (5) betrachten. Dabei werde ich versuchen deren Laufzeit aufgrund einiger Verbesserungen signifikant zu verbessern. Wir werden außerdem versuchen, auf die Frage, ob die dabei verwendeten Werkzeuge auch bei anderen Problemen wieder verwendbar sind, eine Antwort zu geben. Dafür werde ich das Auswahlproblem (6) unter die Lupe nehmen. Zum Schluss ist von Vorteil, die untere Schranke (7) für jedes vergleichsbasierte Sortierverfahren anzusehen, was wir auch tun werden. Gleichzeitig werde ich nach weiteren Sortierverfahren umschauen, die unter bestimmten Bedingungen eine bessere Laufzeit liefern. Darunter fallen Distribution Counting (8) und Radix Sort (9).', 1, 258, 'N', '2014-09-01 20:54:48', '2016-02-08 21:35:28'),
(2, 'Example Message 2', 1, 'Example Message 2', 1, 285, 'Y', '2015-09-22 17:31:31', '2016-02-10 17:25:50');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Daten für Tabelle `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Aktuelle Nachrichten'),
(2, 'Effiziente Algorithmen');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `article_id` smallint(5) unsigned NOT NULL,
  `user_id` smallint(5) unsigned NOT NULL,
  `rating` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `comment`
--

INSERT INTO `comment` (`id`, `comment`, `article_id`, `user_id`, `rating`, `created_at`, `updated_at`) VALUES
(1, 'Hallo,\r\n\r\ntest NAchricht!', 2, 1, 0, '2016-02-07 23:08:44', '2016-02-07 23:08:44'),
(2, 'second', 1, 1, 0, '2016-02-08 01:03:05', '2016-02-08 01:03:05'),
(5, 'fgsfdgdsfgdsfg', 2, 1, 0, '2016-02-09 12:22:27', '2016-02-09 12:22:27');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `alignment` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Daten für Tabelle `menu`
--

INSERT INTO `menu` (`id`, `name`, `url`, `alignment`) VALUES
(1, 'Home', '/articles', 'left'),
(2, 'Sign In', '/membership/signin', 'right'),
(3, 'Register', '/membership/signup', 'right'),
(4, 'Sign Out', '/membership/signout', 'right'),
(5, 'Member''s Area', '/membership/profile', 'right'),
(6, 'Настройки', '/admin', 'right');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(88) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` text COLLATE utf8_unicode_ci NOT NULL,
  `group_id` tinyint(3) unsigned NOT NULL DEFAULT '3',
  `ip` int(10) unsigned NOT NULL,
  `country` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `avatar`, `group_id`, `ip`, `country`, `website`, `updated_at`, `created_at`) VALUES
(1, 'Fonduee aka FewG', '7bn06XtKdbpRyesF5UsbF/s63qWxJdqPLj3XjF2t3kAdcG9qULuKyhD9p2F/gJ5DuGMC1m9LJa94yC/sv2Q17w==', 'test@schepsen.eu', 'http://cdn.myanimelist.net/images/useravatars/4603642.png', 3, 2130706433, NULL, NULL, '2016-02-04 17:37:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `usergroup`
--

CREATE TABLE IF NOT EXISTS `usergroup` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Daten für Tabelle `usergroup`
--

INSERT INTO `usergroup` (`id`, `name`) VALUES
(1, 'Banned'),
(2, 'Guests'),
(3, 'Members'),
(4, 'Moderators'),
(5, 'Administrators');

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
