<?php

ini_set("display_errors", "1");

error_reporting (E_ALL);

$cfg = array(
    'host' => '127.0.0.1',
    'username' => 'root',
    'password' => '1234qwer',
    'database' => 'cms',
    'prefix' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci'
);

foreach(glob('*.sql') as $filename)
{
    echo exec('mysql --default-character-set=utf8 ' . $cfg['database'] . ' -u' . $cfg['username'] . ' -p' . $cfg['password'] .' < ' . $filename . ' 2>&1');
    
    echo 'Installing ' . $filename . '<br>';
}