DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (

  `id` TINYINT(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) NOT NULL,
  `url` VARCHAR(128) NOT NULL,
  `alignment` VARCHAR(5),
    
  PRIMARY KEY (`id`)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `menu` (`name`, `url`, `alignment`) VALUES ('Главная', 'articles', 'left');
INSERT INTO `menu` (`name`, `url`, `alignment`) VALUES ('Вход', '/public/membership/signi', 'right');
INSERT INTO `menu` (`name`, `url`, `alignment`) VALUES ('Регистрация', '/public/membership/signup', 'right');
INSERT INTO `menu` (`name`, `url`, `alignment`) VALUES ('Выход', '/public/membership/signout', 'right');
INSERT INTO `menu` (`name`, `url`, `alignment`) VALUES ('Личный Кабинет', '/public/membership/account', 'right');
INSERT INTO `menu` (`name`, `url`, `alignment`) VALUES ('Настройки', 'admin', 'right');