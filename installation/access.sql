DROP TABLE IF EXISTS `access`;

CREATE TABLE `access` (

  `id` TINYINT(3) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` TINYINT(3) NOT NULL,
  `group_id` TINYINT(3) NOT NULL,
  
  PRIMARY KEY (`id`)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `access` (`menu_id`, `group_id`) VALUES (1, 0);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (1, 1);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (1, 2);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (1, 3);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (1, 5);

INSERT INTO `access` (`menu_id`, `group_id`) VALUES (2, 1);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (3, 1);

INSERT INTO `access` (`menu_id`, `group_id`) VALUES (4, 0);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (4, 2);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (4, 3);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (4, 5);

INSERT INTO `access` (`menu_id`, `group_id`) VALUES (5, 2);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (5, 3);
INSERT INTO `access` (`menu_id`, `group_id`) VALUES (5, 5);

INSERT INTO `access` (`menu_id`, `group_id`) VALUES (6, 5);
