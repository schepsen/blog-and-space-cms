DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (

  `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `category_id` TINYINT(3) UNSIGNED NOT NULL,
  `text` TEXT NOT NULL,
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,
  `views` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `answers` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `hidden` ENUM('N', 'Y') NOT NULL DEFAULT 'Y',
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL,    

  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `article`(`title`, `category_id`, `text`, `user_id`) VALUES ('Example Message', 1, 'Example Message', 1);