DROP TABLE IF EXISTS `user`;

CREATE TABLE IF NOT EXISTS `user` (

  `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(32) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  `email` VARCHAR(64) NOT NULL,
  `group_id` TINYINT(3) UNSIGNED NOT NULL DEFAULT 3,
  `ip` INT(10) UNSIGNED NOT NULL,
  `country` VARCHAR(64),
  `website` VARCHAR(64),
  `updated_at` TIMESTAMP NOT NULL,
  `created_at` TIMESTAMP NOT NULL,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user`(`username`, `password`, `email`, `ip`) VALUES ('FewG', '1234512345', 'test@schepsen.eu', INET_ATON('127.0.0.1'));