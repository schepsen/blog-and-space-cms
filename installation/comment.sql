DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (

  `id` SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `text` TEXT NOT NULL,
  `article_id` SMALLINT(5) UNSIGNED NOT NULL,  
  `user_id` SMALLINT(5) UNSIGNED NOT NULL,  
  `rating` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` DATETIME NOT NULL,
  `updated_at` TIMESTAMP NOT NULL,    

  PRIMARY KEY (`id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `comment`(`text`, `article_id`, `user_id`) VALUES ('Example Comment', 1, 1);