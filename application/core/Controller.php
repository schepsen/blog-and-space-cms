<?php

/**
 * Blog & Space CMS is a php-based Content Management System
 *
 * @author Nikolaj <nikolaj@schepsen.eu> Schepsen
 *
 * @name Blog & Space CMS
 * @version 0.4.0
 *
 * @link https://bitbucket.org/schepsen/php-blog-space-cms
 *
 * @license GNU General Public License, version 2 (GPL-2.0)
 */

class Controller
{
    protected $twig;

    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(TEMPLATE_DIR);  
        
        $this->twig = new Twig_Environment($loader, 
            array
            (
                'auto_reload' => true, 'autoescape' => true
            )
        );
        $this->twig->addGlobal('server', $_SERVER);        
        $this->twig->addGlobal('cms', array
        (                        
            'navigation' => Access::where('group_id', '=', $_SESSION['member']['gid'])
            ->leftJoin('menu', 'access.menu_id', '=', 'menu.id')
            ->select
            (
                'menu.name',
                'menu.url',
                'menu.alignment'
            )
            ->get(),              
            'copyright' => Config::$COPYRIGHT,
            'email' => Config::$EMAIL,
            'repository' => Config::$REPOSITORY,
            'public' => '../../public/',            
                        
            'pwdlength' => Config::$REG_PWD_LENGTH  
        ));        
        
        $this->twig->addGlobal('session', $_SESSION);
    }

    public function model($model)
    {
        require_once MODEL_DIR . $model . '.php'; return new $model();
    }

    public function view($view, $data = [])
    {
        $this->twig->display($view . '.twig', $data);
    }
}

?>