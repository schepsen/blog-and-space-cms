<?php

/**
 * Blog & Space CMS is a php-based Content Management System
 *                         
 * @author Nikolaj <nikolaj@schepsen.eu> Schepsen
 * 
 * @name Blog & Space CMS
 * @version 0.4.0
 * 
 * @link https://bitbucket.org/schepsen/php-blog-space-cms
 * 
 * @license GNU General Public License, version 2 (GPL-2.0)
 */

class Core
{   
    protected $controller = 'articles', $method = 'index', $params;

    public function __construct()
    {        
        $parsedURL = $this->parseRequest();

        if(file_exists(CONTROLLER_DIR . $parsedURL[0] . '.php'))
        {
            $this->controller = $parsedURL[0]; unset($parsedURL[0]);
        }

        require_once CONTROLLER_DIR . $this->controller . '.php';

        $this->controller = new $this->controller();

        if(isset($parsedURL[1]))
        {
            if(method_exists($this->controller, $parsedURL[1]))
            {
                $this->method = $parsedURL[1]; unset($parsedURL[1]);
            }
        }

        $this->params = $parsedURL ? array_values($parsedURL) : [];
        
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    protected function parseRequest()
    {
        if(isset($_GET['url']))
        {
            return explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }
};