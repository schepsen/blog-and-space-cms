<?php
/*
 * Paths
 */
define('ROOT_DIR', dirname(getcwd()) . '/');
/*
 * Application's Directory
 */
define('APP_DIR', ROOT_DIR . 'application/');
/*
 * Eloquent Model's Directory
 */
define('MODEL_DIR', APP_DIR . 'models/');
/*
 * View's Directory
 */
define('VIEW_DIR', APP_DIR . 'views/');
/*
 * Vendor's Directory
 */
define('VENDOR_DIR', ROOT_DIR . 'vendor/');
/*
 * Twig Template's Directory
 */
define('TEMPLATE_DIR', VIEW_DIR);
/*
 * Controller's Directory
 */
define('CONTROLLER_DIR', APP_DIR . 'controllers/');

/*
 * Id Usergroup's Name
 * 
 * 1	Banned
 * 2	Guests
 * 3	Members
 * 4	Moderators
 * 5	Administrators
 */

class Role
{
    static $BANNED = 1;    
    static $GUEST = 2; 
    static $MEMBER = 3; 
    static $MODER = 4; 
    static $ADMIN = 5; 
};

/*
 * Main Settings for Blog & Space CMS
 */

class Config
{      
    static $APP_DOMAIN = 'http://beta.schepsen.eu';
    
    static $DEFAULT = array
    (
        'CONTROLLER' => 'articles',
        'METHOD' => 'index'
    );
    /*
     * Type of the used Database
     */
    static $DB_DRIVER = 'mysql';
    /*
     * MySQL Database Hostname
     */
    static $DB_HOST = '127.0.0.1';
    /*
     * MySQL Database Username
     */
    static $DB_USERNAME = 'root';
    /*
     * MySQL Database Password
     */
    static $DB_PASSWORD = '1234ASdf';
    /*
     * The Name of the Database for Blog & Space CMS
     */
    static $DB_NAME = 'bgs';
    static $DB_PREFIX = '';
    
    static $SUCCESS = 'notifications/success';
    static $EXCEPTION = 'notifications/exception';

    static $DEBUG = true;
    
    static $REG_UNAME_MIN = 4;
    static $REG_UNAME_MAX = 16;
    
    static $REG_PWD_LENGTH = 8;
    
    static $COPYRIGHT = 'Copyright © 2010-2015 Nikolaj Schepsen';    
    static $EMAIL = 'support (at) schepsen.eu';    
    
    static $REPOSITORY = 'https://bitbucket.org/schepsen/php-blog-space-cms';   
};

class Language
{
    static $ACCESS_DENIED = 'Access Denied!';

    static $EMPTY_FIELDS = 'To proceed, you have to fill out all fields';
    static $WP_OR_NO_USER = 'Supplied password does not match user password or username does not exist';
    
    static $AGREE_TERMS = 'To proceed, you need to agree to the terms and conditions';
    
    static $PAGE_NOT_SELECTED = 'To proceed, you have to select an article.';
    static $PAGE_NOT_FOUND = 'The page you requested was not found!';
    
    static $LOGGED_IN = 'You have been successfully logged in';
    static $LOGGED_OUT = 'You have been successfully logged out';
    static $SIGNED_UP = 'Your account has been created successfully and is ready to use';
    
    static $COMMENT_CREATED = 'The comment is created susseccfully!';
    
    static $ALREADY_LOGGED_IN = 'You are already logged in. Access denied!';
};
