<?php

class Articles extends Controller
{
    public function index()
    {
        try
        {
            $this->view('articles/index', array(
                'articles' => Article::select
                (   // Article ID
                    'article.id',  
                    // Article Title
                    'article.title',
                    // Category ID
                    'article.category_id as catid',
                    // Author ID
                    'article.user_id as uid',
                    // Article Views Count
                    'article.views',
                    // Article's Creation Time
                    'article.created_at',
                    // Author's Nickname
                    'user.username',
                    // Article Comments Count
                    new Illuminate\Database\Query\Expression('count(comment.article_id) as answers')
                )
                ->leftJoin('user', 'user.id', '=', 'article.user_id')
                ->leftJoin('comment', 'article.id', '=', 'comment.article_id')
                ->groupBy('article.id')->orderBy('article.id', 'desc')->get()
            ));
        }
        catch(Exception $e)
        {
            $this->view('notifications/exception', array('message' => $e->getMessage()));
        }
    }

    public function comment()
    {      
        try
        {             
            if(!isset($_SESSION['id']) || !isset($_POST['name']) || $_POST['name'] != 'comment' || (($id = intval($_POST['aid'])) <= 0))
            {
                throw new Exception(Language::$ACCESS_DENIED);
            }
            
            if(strlen($_POST['comment']) < 3)
            {
                throw new Exception(Language::$EMPTY_FIELDS);
            }
            
            $comment = new Comment(
                array
                (
                    'comment' => $_POST['comment'],
                    'article_id' => $id,
                    'user_id' => $_SESSION['id']
                )
            );             
            $comment->save();   
            
            $this->view('notifications/success', 
                array
                (
                    'message' => Language::$COMMENT_CREATED                                    
                )
            );
        }
        catch(Exception $e)
        {
            $this->view('notifications/exception', array('message' => $e->getMessage()));
        }
    }

    public function read($id = -1)
    {
        try
        {
            if(($id = intval($id)) <= 0)
            {
                throw new Exception(Language::$PAGE_NOT_FOUND);
            }

            $article = Article::findOrFail($id);      
            
            $this->view('articles/read', array(                
                'article' => $article,
                'account' => User::select(array
                    (                                    
                        'avatar'                                    
                    )
                )->where('id', '=', $_SESSION['id'])->first(),                       
                'comments' => $article->comments
            ));
            
            Article::find($id)->increment('views');
        }
        catch(Illuminate\Database\Eloquent\ModelNotFoundException $e)
        {
            $this->view('notifications/exception', array
                    (
                        'message' => Language::$PAGE_NOT_FOUND
                    )
                    );
        }
        catch(Exception $e)
        {
            $this->view('notifications/exception', array('message' => $e->getMessage()));
        }
    }
}

?>