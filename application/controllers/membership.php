<?php

use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;

class membership extends Controller
{
    public function index() 
    {
        $this->profile();
    }
    
    public function signin()
    {
        try
        {   
            if(intval($_SESSION['member']['gid']) != Role::$GUEST)
            {
                header
                (
                    'Location: ' . Config::$APP_DOMAIN
                ); 
                
                return;
            }            
            /*
             * Checks the Request's Type (POST or GET)
             */
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {            
                if(!isset($_POST['username']) || !isset($_POST['password']))
                {
                    throw new Exception(Language::$EMPTY_FIELDS);
                }     
                
                $member = User::select
                (
                    'id',
                    'username',
                    'email',
                    'avatar',
                    'group_id as gid'
                )
                ->where('username', '=', $_POST['username'])
                ->where('password', '=', base64_encode(hash  
                        
                    (
                        'whirlpool', $_POST['password'], TRUE))
                    )             
                    
                ->firstOrFail();
     
                $_SESSION['member'] = $member->toArray();
                
                $this->view(Config::$SUCCESS, ['message' => Language::$LOGGED_IN]);                
            }
            else
            {
                $this->view('membership/signin');
            }
        }
        catch (Exception $e)
        {
            $this->view(Config::$EXCEPTION, array
                (
                    'message' => ($e instanceof ModelNotFoundException) ? Language::$WP_OR_NO_USER : $e->getMessage()
                )   
            );
        }
    }

    public function signup()
    {
        try
        {
            if(intval($_SESSION['member']['gid']) != Role::$GUEST)
            {
                header
                (
                    'Location: ' . Config::$APP_DOMAIN
                ); 
                
                return;
            }       
   
            if($_SERVER['REQUEST_METHOD'] == 'POST')
            {            
                if(!$_POST['agreement'])
                {
                    throw new Exception(Language::$AGREE_TERMS);
                }
                
                if((strlen($_POST['username']) < Config::$REG_UNAME_MIN) || (strlen($_POST['username']) > Config::$REG_UNAME_MAX))
                {
                    throw new Exception
                    (
                        'To proceed, your username must be between '
                        . Config::$REG_UNAME_MIN . 
                        ' and ' 
                        . Config::$REG_UNAME_MAX . ' characters long'
                    );
                }
                
                if(strlen($_POST['password']) < Config::$REG_PWD_LENGTH)
                {
                    throw new Exception
                    (
                        'To proceed, your password must be at least '
                        . Config::$REG_PWD_LENGTH . ' characters long'
                    );
                }
                
                if($_POST['password'] != $_POST['confirmation'])
                {
                    throw new Exception('Your password entries did not match');
                }
                
                if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                {
                    throw new Exception
                    (
                        'To proceed, your need a valid email address'
                    );
                }
                
                if(User::where('username', '=', $_POST['username'])->count() != 0)
                {
                    throw new Exception
                    (
                        'Sorry, that username is already taken'
                    );
                }
                
                $member = new User(
                    array
                    (
                        'username' => $_POST['username'],
                        'password' => base64_encode(hash
                                (
                                    'whirlpool', $_POST['password'], TRUE
                                )
                            ),
                        'email' => $_POST['email'],
                        'ip' => ip2long($_SERVER['REMOTE_ADDR']),
                        'country' => $_POST['country'],
                        'website' => $_POST['website']                        
                    )
                );
                
                $member->save(); 
                
                $this->view(Config::$SUCCESS, ['message' => Language::$SIGNED_UP]);   
            }            
            else
            {                
                $this->view('membership/signup');
            }
        }
        catch(Exception $e)
        {
            $this->view(Config::$EXCEPTION, array('message' => $e->getMessage()));
        }
    }

    public function signout()
    {
        session_destroy(); 
        session_unset();
        
        $this->view(Config::$SUCCESS, array('message' => Language::$LOGGED_OUT));
    }

    public function profile($id = -1)
    {     
        try
        {
            if(($id = intval($id)) <= 0)
            {
                if(is_null($id = $_SESSION['member']['id'] ? : null))
                {
                    header
                    (
                        'Location: ' . Config::$APP_DOMAIN
                    );
            
                    return;
                }
            }
            
            $this->view('membership/profile', array
                (
                    'account' => User::select(array
                         (
                             'username as name', 
                             'email',
                             'avatar',
                             'created_at as signed'                                         
                         )                             
                    )
                    ->findOrFail($id)
                )
            );
        }
        catch(Exception $e)
        {
            $this->view(Config::$EXCEPTION, array
                (
                    'message' => ($e instanceof ModelNotFoundException) ? Language::$PAGE_NOT_FOUND : $e->getMessage()
                )   
            );
        }     
    }
}
?>