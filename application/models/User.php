<?php

class User extends Illuminate\Database\Eloquent\Model
{    
    /**
     * The table associated with the model
     */  
    
    protected $table = 'user';    
    
    /**
     * The attributes that are mass assignable
     */
    
    protected $fillable = array
    (
        'username', 'password', 'email', 'ip', 'country', 'website'
    );     

    /**
     * Get the Member's Articles
     */
    
    public function articles()
    {
        return $this->hasMany(Article::class, 'user_id', 'id');
    }
    
    /**
     * Get the Member's Comments
     */
    
    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_id', 'id');
    }
    
    /**
     * Find by username and by password, or throw an exception
     *
     * @param string $username The Username
     * @param string $password The Password
     * @param mixed $columns The columns to return
     *
     * @throws ModelNotFoundException if no matching User exists
     *
     * @return User
     */
    
    public function findMemberOrFail($username, $password, $columns = array('*'))
    {
        if (!is_null
                (
                    $user = $this->where('username', '=', $username)->where('password', '=', $password)->first($columns))
                ) 
        {
            return $user;
        }
        
        throw new ModelNotFoundException;
    }
}
