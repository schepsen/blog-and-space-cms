<?php

class Comment extends Illuminate\Database\Eloquent\Model
{
    /**
     * The table associated with the model
     */
    
    protected $table = 'comment';
    
    /**
     * The attributes that are mass assignable
     */
    
    protected $fillable = array
    (
        'comment', 'article_id', 'user_id'
    );
    
    /**
     * Get the Comment's Author Relationship
     */
    
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    /**
     * Get the Comment's Article Relationship
     */
    
    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }
}