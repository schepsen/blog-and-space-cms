<?php

class Article extends Illuminate\Database\Eloquent\Model
{
    /**
     * The table associated with the model
     */    
    
    protected $table = 'article';  
    
    /**
     * The attributes that are mass assignable
     */
    
    protected $fillable = array
    (
        'title', 'category_id', 'text', 'user_id'        
    );
    
    /**
     * Get the Article's Author Relationship
     */
    
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    /**
     * Get the Article's Category Relationship
     */
    
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
    
    /**
     * Get the comments for the blog article
     */
    
    public function comments()
    {
        return $this->hasMany(Comment::class, 'article_id', 'id')
            ->with(array
                    (
                        'author' => function($query)
                            {
                                $query->select('id', 'username');
                            }
                    )
                )
            ->select(
                'id',
                'comment',                    
                'rating',
                'user_id',
                'created_at as date'
                    );
    }
}