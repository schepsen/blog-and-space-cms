<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Menu extends Eloquent
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    
    public $timestamps = false;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */  
    
    protected $table = 'menu';    

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    
    protected $guarded = array('id');
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = array('name', 'url', 'alignment', 'visibility');   
    
}
