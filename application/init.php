<?php

use Illuminate\Database\Capsule\Manager as Capsule;

session_start();
/*
 * Requires the config file
 */
require_once 'config.php';
/*
 * Chechs if the web's app runs in DEBUG MODE
 */
if(Config::$DEBUG)
{
    ini_set('display_errors', '1');
    error_reporting(E_ALL & ~E_NOTICE);
}
/*
 * Requires the Composer's AUTOLOADER
 */
require_once VENDOR_DIR . 'autoload.php';
/*
 * Requires the main application CLASS
 */
require_once 'core/Core.php';
/*
 * Requires the main controller CLASS
 */
require_once 'core/Controller.php';

$capsule = new Capsule();

$capsule->addConnection(array
    (    
        'driver' => Config::$DB_DRIVER,
        'host' => Config::$DB_HOST,
        'username' => Config::$DB_USERNAME,
        'password' => Config::$DB_PASSWORD,
        'database' => Config::$DB_NAME,
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => Config::$DB_PREFIX                
    ));

$capsule->bootEloquent();

/*
 * Id Usergroup's Name
 * 
 * 1	Banned
 * 2	Guests
 * 3	Members
 * 4	Moderators
 * 5	Administrators
 */

if(!isset($_SESSION['member'])) $_SESSION['member']['gid'] = Role::$GUEST;
